############################################
## Configuration for fast Commands Block ###
############################################
# Configuration for fast commands block
TRIGGER_SOURCE      = 2 # 1=IPBus, 2=Test-FSM, 3=TTC, 4=TLU, 5=Ext, 6=Hit-Or, 7=User-defined frequency
AUTOZERO_SOURCE     = 1 # 1=IPBus, 2=Test-FSM, 3=Free-Run
IPB_FAST_DURATION   = 1 
TRIGGERS_TO_ACCEPT  = 0x00001 # 0 = Unlimited triggers
GLB_PULSE_DATA      = 0x5C 
CAL_DATA_PRIME      = 983104
CAL_DATA_INJECT     = 1015808 
INIT_ECR_EN         = 0 
BACKPRESSURE_EN     = 0 

TRIGGER_DURATION = 0

# Enable bits for test pulse fsm
TP_FSM_ECR_EN           = 0 
TP_FSM_TEST_PULSE_EN    = 1
TP_FSM_INJECT_PULSE_EN  = 1
TP_FSM_TRIGGER_EN       = 1 
# Delays for test pulse fsm
DELAY_AFTER_ECR           = 5 
DELAY_AFTER_AUTOZERO      = 0 
DELAY_AFTER_PRIME_PULSE   = 16 
DELAY_AFTER_INJECT_PULSE  = 16
DELAY_BEFORE_NEXT_PULSE   = 512 
# Auto-Zero parameters
AUTOZERO_FREQ         = 100 
VETO_AFTER_AUTOZERO   = 10 
VETO_EN               = 0 
############################################
# Ext TLU and DIO5 Configuration
DIO5_ENABLE        = 0 
EXT_CLK_EN         = 0
TLU_ENABLE         = 0 
TLU_HNDSHAKE_MODE  = 0 
DIO5_CHOUT_EN      = 0 
DIO5_TERM_EN       = 0b11111
DIO5_CH1THR        = 0 
DIO5_CH2THR        = 100 #0xFF 
DIO5_CH3THR        = 0x0F 
DIO5_CH4THR        = 0x0F 
DIO5_CH5THR        = 0x0F 
############################################
# Readout Block Configuration
READOUT_DATA_HANDSHAKE_EN  = 0 
L1A_TIMEOUT                = 4000 
HYBRID_ENABLE              = 0b1
CHIPS_ENABLE               = 0b1
N_EVENTS_READOUT           = 15
############################################
# Register-Readback Configuration
AUTOREAD_ADDR_A = 0
AUTOREAD_ADDR_B = 0
############################################
#Aurora Block Configuration
AURORA_ERROR_MODULE_ADDR = 0
AURORA_ERROR_CHIP_ADDR   = 0
