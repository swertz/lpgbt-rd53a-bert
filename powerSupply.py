#!/usr/bin/env python

import serial
import sys
import time

class PowerSupply(object):
    def __init__(self, verbose=False):
        self.verbose = verbose
        self.ser = serial.Serial('/dev/ttyUSB0', baudrate=9600, xonxoff=1, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE)
        if self.verbose:
            self._write('*IDN?')
            print("Initialized PS: {}".format(self._read()))

    def _write(self, s):
        cmd = bytearray(s + '\n', 'ascii') # warning: use 'bytes' in python3
        self.ser.write(cmd)

    def _read(self):
        return self.ser.readline()

    def readV(self, chan):
        start = time.time()
        self._write('V{}O?'.format(chan))
        repl = self._read()
        stop = time.time()
        voltage = float(repl.strip().strip('V'))
        if self.verbose:
            print("Voltage channel {}: {:.3f}V -- read in {:.5f}ms".format(chan, voltage, 1000*(stop-start)))
        return voltage

    def setV(self, chan, volt):
        assert(volt >= 0 and volt < 3)
        self._write('V{} {}'.format(chan, volt))

    def readSetV(self, chan):
        self._write('V{}?'.format(chan))
        repl = self._read()
        voltage = float(repl.strip().split(' ')[1].strip('V'))
        if self.verbose:
            print("Voltage channel {} set to: {:.3f}V".format(chan, voltage))
        return voltage

    def on(self, chan):
        target = self.readSetV(chan)
        if self.verbose:
            print("Turning on channel {} to {}V".format(chan, target))
        start = time.time()
        self._write('OP{} 1'.format(chan))
        while True:
            time.sleep(0.01)
            readV = self.readV(chan)
            if abs(readV - target) / target < 0.01:
                break
        # ser.write(b'*OPC?\n')
        # print("Reply: {}".format(ser.readline()))
        # time.sleep(0.25)
        # getVoltage(chan)
        stop = time.time()
        if self.verbose:
            print("Done in {:.5f}ms".format(1000*(stop - start)))

    def off(self, chan):
        if self.verbose:
            print("Turning off channel {}".format(chan))
        start = time.time()
        self._write('OP{} 0'.format(chan))
        # ser.write(b'*OPC?\n')
        # print("Reply: {}".format(ser.readline()))
        # time.sleep(0.25)
        # getVoltage(chan)
        stop = time.time()
        if self.verbose:
            print("Done in {:.5f}ms".format(1000*(stop - start)))

if __name__ == '__main__':
    if len(sys.argv) >= 3:
        chan = sys.argv[1]
        assert(chan == '1' or chan == '2')

        ps = PowerSupply(verbose=True)

        if (sys.argv[2] == 'on'):
            ps.on(chan)
        elif (sys.argv[2] == 'off'):
            ps.off(chan)
        elif (sys.argv[2] == 'onoff'):
            ps.on(chan)
            time.sleep(float(sys.argv[3]))
            ps.off(chan)
        elif sys.argv[2] == 'readV':
            ps.readV(chan)
        elif sys.argv[2] == 'readSetV':
            ps.readSetV(chan)
        elif sys.argv[2] == 'setV':
            ps.setV(chan, float(sys.argv[3]))
