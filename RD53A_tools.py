from __future__ import division, print_function, absolute_import

from libpyDTC.fc7_daq import Write_chipReg

# voltage trim for board 210
VOLTAGE_TRIM = 0b10000 << 5 | 0b11000

# voltage trim for board 176
# VOLTAGE_TRIM = 0b11000 << 5 | 0b10110
# VOLTAGE_TRIM = 10 << 5 | 10

def configureCML(lanes, tap1=False, tap2=False, invTap1=False, invTap2=False):
    """ Enable CML driver lanes, configure taps

    Args:
        lanes: list of lanes (0-3) to enable
        tap1 (default False): enable TAP1
        tap2 (default False): enable TAP2
        invTap1 (default False): invert TAP1
        invTap2 (default False): invert TAP2
    """
    Write_chipReg(31, VOLTAGE_TRIM)
    # Write_chipReg(64, 537)
    # Write_chipReg(63, 1)

    # enable lanes, configure pre-emphasis
    laneCfg = 0
    for l in lanes:
        assert(l >= 0 and l <= 3)
        laneCfg |= 1 << l
    Write_chipReg(69, tap2<<7 | tap1<<6 | invTap2<<5 | invTap1<<4 | laneCfg )

def sendClock(lane=0):
    """Send 640 Mbps clock signal on lane (other lanes send clock by default)"""
    Write_chipReg(68, 0 << (2 * lane))

def sendPRBS7(lane=0):
    """Send PRBS7 on lane (other lanes send clock by default)"""
    Write_chipReg(68, 0b10 << (2 * lane))

def sendAurora(lane=0):
    """Send Aurora data on lane (other lanes send clock by default)"""
    Write_chipReg(68, 1 << (2 * lane))

def sendGround(lane=0):
    """Send ground on lane (other lanes send clock by default)"""
    Write_chipReg(68, 0b11 << (2 * lane))

def configureLanes(*lanes):
    """Configure what each lane should send:
        0: 640 Mbps clock
        1: Aurora data
        2: PRBS7
        3: ground
        Example: configureLanes(1, 1, 2, 2) -> send data on lanes 0 and 1, send PRBS on lanes 2 and 3
    """
    assert(len(lanes) == 4)
    assert(all((l >= 0 and l <= 3 for l in lanes)))
    laneCfg = 0
    for i, l in enumerate(lanes):
        laneCfg |= l << (2 * i)
    Write_chipReg(68, laneCfg)

def setTap0(dac):
    """Set TAP0 bias current (0-1023; max ~ 14 mA)"""
    assert(dac >= 0 and dac < 1024)
    Write_chipReg(70, dac)

def setTap1(dac):
    """Set TAP1 bias current (0-1023; max ~ 14 mA)"""
    assert(dac >= 0 and dac < 1024)
    Write_chipReg(71, dac)

def setTap2(dac):
    """Set TAP2 bias current (0-1023; max ~ 14 mA)"""
    assert(dac >= 0 and dac < 1024)
    Write_chipReg(72, dac)

